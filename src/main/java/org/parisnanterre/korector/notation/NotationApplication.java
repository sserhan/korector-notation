package org.parisnanterre.korector.notation;

import org.parisnanterre.korector.notation.entity.Metric;
import org.parisnanterre.korector.notation.services.MetricService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableEurekaClient
public class NotationApplication {
	public NotationApplication(MetricService metricService) {
		NotationApplication.metricService = metricService;
	}

	public static void main(String[] args) {
		SpringApplication.run(NotationApplication.class, args);
	}

	private static final String metricSonar = "duplicated_lines_density,code_smells,sqale_rating,sqale_index,bugs,reliability_rating,vulnerabilities,security_rating,security_hotspots,branch_coverage,tests";
	private static MetricService metricService;

	@PostConstruct
	public static void init(){
		List<String> listMetrics = Arrays.asList(metricSonar.split(","));
		listMetrics
				.forEach(s -> {
					Metric metric = new Metric();
					metric.setProvenance("Sonarqube");
					metric.setNom(s);
					switch (s){
						case "duplicated_lines_density":
							metric.setUnite("%");
							metric.setAlias("Duplicated Lines %");
							break;
						case "branch_coverage":
							metric.setUnite("%");
							metric.setAlias("Condition Coverage %");
							break;
						case "code_smells":
							metric.setUnite("max");
							metric.setAlias("Code Smell");
							break;
						case "sqale_index":
							metric.setUnite("max");
							metric.setAlias("Technical Debt (min)");
							break;
						case "bugs":
							metric.setUnite("max");
							metric.setAlias("Bugs");
							break;
						case "vulnerabilities":
							metric.setUnite("max");
							metric.setAlias("Vulnerabilites");
							break;
						case "security_hotspots":
							metric.setUnite("max");
							metric.setAlias("Security hotspots");
							break;
						case "tests":
							metric.setUnite("min");
							metric.setAlias("Tests");
							break;
						case "sqale_rating":
							metric.setUnite("/");
							metric.setAlias("Maintainability rating");
							metric.setValeurComparaison(5.0);
							break;
						case "security_rating":
							metric.setUnite("/");
							metric.setAlias("Security rating");
							metric.setValeurComparaison(5.0);
							break;
						case "reliability_rating":
							metric.setUnite("/");
							metric.setAlias("Reliability rating");
							metric.setValeurComparaison(5.0);
							break;
					}
					metricService.saveGlobal(metric);
				});
	}
}
