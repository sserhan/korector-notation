package org.parisnanterre.korector.notation.api;

import org.parisnanterre.korector.notation.entity.Note;
import org.parisnanterre.korector.notation.payload.ModelMapper;
import org.parisnanterre.korector.notation.payload.response.NoteResponse;
import org.parisnanterre.korector.notation.payload.request.NoteForm;
import org.parisnanterre.korector.notation.services.MetricService;
import org.parisnanterre.korector.notation.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="/api/v1")
public class NoteController {
    @Autowired
    private NoteService noteService;
    @Autowired
    private MetricService metricService;

    @GetMapping("/notes")
    public List<NoteResponse> getAllNotes(){
        List<NoteResponse> noteResponses = new ArrayList<>();
        noteService.getAll().forEach( note -> {
            noteResponses.add(ModelMapper.noteToNoteResponse(note));
        });
        return noteResponses;
    }

    @GetMapping("/notes/{id}")
    public NoteResponse getOneNotes(@PathVariable Long id){
        return ModelMapper.noteToNoteResponse(noteService.getOne(id));
    }

    @PostMapping("/notes/noteTotal")
    public NoteResponse genererNote(@RequestBody NoteForm noteForm){
        Note note = ModelMapper.noteFormToNote(noteForm);
        note.setNoteTotal(noteService.genererNote(note.getMetrics()));
        return ModelMapper.noteToNoteResponse(note);
    }

    @PostMapping("/notes")
    public NoteResponse saveNote(@RequestBody NoteForm noteForm){
        Note note = ModelMapper.noteFormToNote(noteForm);
        note.setMetrics(metricService.getNewListMetrics(note.getMetrics()));
        return ModelMapper.noteToNoteResponse(noteService.save(note));
    }

    @PutMapping("/notes")
    public NoteResponse updateNote(@RequestBody NoteForm noteForm){
        Note note = ModelMapper.noteFormToNote(noteForm);
        note.setNoteTotal(noteService.genererNote(note.getMetrics()));
        return ModelMapper.noteToNoteResponse(noteService.save(note));
    }

    @DeleteMapping("/notes/{id}")
    public void deleteNote(@PathVariable Long id){
        noteService.delete(id);
    }

}
