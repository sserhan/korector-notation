package org.parisnanterre.korector.notation.api;

import org.parisnanterre.korector.notation.payload.ModelMapper;
import org.parisnanterre.korector.notation.payload.response.MetricResponse;
import org.parisnanterre.korector.notation.payload.request.MetricForm;
import org.parisnanterre.korector.notation.services.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="/api/v1")
public class MetricController {
    @Autowired
    private MetricService metricService;

    @GetMapping("/metrics")
    public List<MetricResponse> getAllMetrics(){
        List<MetricResponse> metricResponses = new ArrayList<>();
        metricService.getAll().forEach( metric -> {
            metricResponses.add(ModelMapper.metricToMetricResponse(metric));
        });
        return metricResponses;
    }

    @GetMapping("/metrics/provenance")
    public List<MetricResponse> getAllFrom(String provenance){
        List<MetricResponse> metricResponses = new ArrayList<>();
        metricService.getMetricFrom(provenance).forEach( metric -> {
            metricResponses.add(ModelMapper.metricToMetricResponse(metric));
        });
        return metricResponses;
    }

    @GetMapping("/metrics/{id}")
    public MetricResponse getOneMetric(@PathVariable Long id){
        return ModelMapper.metricToMetricResponse(metricService.getOne(id));
    }

    @PostMapping("/metrics")
    public MetricResponse saveMetric(@RequestBody MetricForm metricForm){
        return ModelMapper.metricToMetricResponse(
                metricService.saveGlobal(ModelMapper.metricFormToMetric(metricForm)));
    }

    @PutMapping("/metrics")
    public MetricResponse updateMetric(@RequestBody MetricForm metricForm){
        return ModelMapper.metricToMetricResponse(
                metricService.saveGlobal(ModelMapper.metricFormToMetric(metricForm)));
    }

    @DeleteMapping("/metrics/{id}")
    public void deleteMetric(@PathVariable Long id){
        metricService.delete(id);
    }
}
