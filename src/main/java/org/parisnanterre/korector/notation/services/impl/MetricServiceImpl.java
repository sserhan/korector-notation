package org.parisnanterre.korector.notation.services.impl;

import org.parisnanterre.korector.notation.entity.Metric;
import org.parisnanterre.korector.notation.repository.MetricRepository;
import org.parisnanterre.korector.notation.services.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MetricServiceImpl implements MetricService {
    @Autowired
    private MetricRepository metricRepository;

    @Override
    public List<Metric> getAll() {
        return metricRepository.findByGlobal(true);
    }

    @Override
    public Metric getOne(Long id) {
        return metricRepository.getOne(id);
    }

    @Override
    public Metric save(Metric metric) {
        return metricRepository.save(metric);
    }

    @Override
    public Metric saveGlobal(Metric metric){
        metric.setGlobal(true);
        return metricRepository.save(metric);
    }

    @Override
    public void delete(Long id) {
        metricRepository.deleteById(id);
    }

    @Override
    public List<Metric> getMetricFrom(String provenance) {
        return metricRepository.findByProvenanceAndGlobal(provenance,true);
    }

    @Override
    public List<Metric> getNewListMetrics(List<Metric> metrics){
        List<Metric> newMetrics = new ArrayList<>();
        metrics.forEach(m -> {
            Metric metric = new Metric();
            metric.setNom(m.getNom());
            metric.setAlias(m.getAlias());
            metric.setValeur(m.getValeur());
            metric.setGlobal(false);
            metric.setValeurComparaison(m.getValeurComparaison());
            metric.setUnite(m.getUnite());
            metric.setProvenance(m.getProvenance());
            metric.setPoids(m.getPoids());
            newMetrics.add(metric);
        });
        return newMetrics;
    }
}
