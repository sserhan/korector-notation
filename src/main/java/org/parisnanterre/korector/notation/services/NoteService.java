package org.parisnanterre.korector.notation.services;

import org.parisnanterre.korector.notation.entity.Metric;
import org.parisnanterre.korector.notation.entity.Note;

import java.util.List;

public interface NoteService {
    List<Note> getAll();
    Note getOne(Long id);
    Note save(Note note);
    void delete (Long id);
    Double genererNote(List<Metric> metrics);
}
