package org.parisnanterre.korector.notation.services;

import org.parisnanterre.korector.notation.entity.Metric;

import java.util.List;

public interface MetricService {
    List<Metric> getAll();
    Metric getOne(Long id);
    Metric save (Metric metric);
    Metric saveGlobal(Metric metric);
    List<Metric> getNewListMetrics(List<Metric> metrics);
    void delete(Long id);
    List<Metric> getMetricFrom(String provenance);
}
