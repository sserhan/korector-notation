package org.parisnanterre.korector.notation.services.impl;

import org.parisnanterre.korector.notation.entity.Metric;
import org.parisnanterre.korector.notation.entity.Note;
import org.parisnanterre.korector.notation.repository.NoteRepository;
import org.parisnanterre.korector.notation.services.MetricService;
import org.parisnanterre.korector.notation.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class NoteServiceImpl implements NoteService {
    @Autowired
    private MetricService metricService;
    @Autowired
    private NoteRepository noteRepository;

    @Override
    public List<Note> getAll() {
        return noteRepository.findAll();
    }

    @Override
    public Note getOne(Long id) {
        return noteRepository.getOne(id);
    }

    @Override
    public Note save(Note note) {
        return noteRepository.save(note);
    }

    @Override
    public void delete(Long id) {
        noteRepository.deleteById(id);
    }

    @Override
    public Double genererNote(List<Metric> metrics) {
        return traitementMetrique(metrics);
    }

    private Double traitementMetrique(List<Metric> metrics){
        double total = 0.0;
        metrics = metrics.stream()
                .filter(m -> m.getValeur() != null)
                .filter(m -> m.getPoids() != null)
                .filter(m -> m.getPoids() != 0)
                .collect(Collectors.toList());
        for(Metric m : metrics){
            if(m.getValeur() == null) {
                m.setValeur(0.0);
            }
            if(m.getUnite() == null){
                total += m.getValeur() * m.getPoids();
            }else {
                switch (m.getUnite()) {
                    case "%":
                        total += m.getValeur() * m.getPoids();
                        break;
                    case "max":
                        if (m.getValeur() <= m.getValeurComparaison()) {
                            total += 100.0 * m.getPoids();
                            break;
                        }
                    case "min":
                        if (m.getValeur() >= m.getValeurComparaison()){
                            total += 100.0 * m.getPoids();
                            break;
                        }
                    case "/":
                        if(m.getValeur().equals(m.getValeurComparaison())) total += 0.0;
                        double val = m.getValeurComparaison() -m.getValeur();
                        total += (val*100/(m.getValeurComparaison()-1)) * m.getPoids();
                        break;
                    default:
                        total += 0.0;
                }
            }
        }
        return total/metrics.stream().mapToDouble(Metric::getPoids).sum();
    }
}
