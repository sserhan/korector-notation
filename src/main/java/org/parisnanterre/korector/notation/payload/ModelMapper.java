package org.parisnanterre.korector.notation.payload;

import org.parisnanterre.korector.notation.entity.Metric;
import org.parisnanterre.korector.notation.entity.Note;
import org.parisnanterre.korector.notation.payload.response.MetricResponse;
import org.parisnanterre.korector.notation.payload.response.NoteResponse;
import org.parisnanterre.korector.notation.payload.request.MetricForm;
import org.parisnanterre.korector.notation.payload.request.NoteForm;

import java.util.ArrayList;
import java.util.List;

public class ModelMapper {

    public static NoteResponse noteToNoteResponse(Note note){
        NoteResponse noteResponse = new NoteResponse();
        noteResponse.setId(note.getId());
        noteResponse.setNoteTotal(note.getNoteTotal());
        List<MetricResponse> metricResponses = new ArrayList<>();
        note.getMetrics().forEach( m -> metricResponses.add(metricToMetricResponse(m)));
        noteResponse.setMetrics(metricResponses);
        return noteResponse;
    }

    public static Note noteFormToNote(NoteForm noteForm){
        Note note = new Note();
        note.setId(noteForm.getId());
        note.setNoteTotal(noteForm.getNoteTotal());
        List<Metric> metrics = new ArrayList<>();
        noteForm.getMetrics().forEach(m -> metrics.add(metricFormToMetric(m)));
        note.setMetrics(metrics);
        return note;
    }

    public static MetricResponse metricToMetricResponse(Metric metric){
        MetricResponse metricResponse = new MetricResponse();
        metricResponse.setId(metric.getId());
        metricResponse.setNom(metric.getNom());
        metricResponse.setUnite(metric.getUnite());
        metricResponse.setProvenance(metric.getProvenance());
        metricResponse.setPoids(metric.getPoids());
        metricResponse.setValeur(metric.getValeur());
        metricResponse.setValeurComparaison(metric.getValeurComparaison());
        metricResponse.setAlias(metric.getAlias());
        return metricResponse;
    }

    public static Metric metricFormToMetric(MetricForm metricForm){
        Metric metric = new Metric();
        metric.setId(metricForm.getId());
        metric.setNom(metricForm.getNom());
        metric.setUnite(metricForm.getUnite());
        metric.setProvenance(metricForm.getProvenance());
        metric.setPoids(metricForm.getPoids());
        metric.setValeur(metricForm.getValeur());
        metric.setValeurComparaison(metricForm.getValeurComparaison());
        metric.setAlias(metricForm.getAlias());
        return metric;
    }
}
