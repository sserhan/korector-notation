package org.parisnanterre.korector.notation.payload.response;

import org.parisnanterre.korector.notation.payload.response.MetricResponse;

import java.io.Serializable;
import java.util.List;

public class NoteResponse implements Serializable {
    private Long id;
    private Double noteTotal;
    private List<MetricResponse> metrics;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getNoteTotal() {
        return noteTotal;
    }

    public void setNoteTotal(Double noteTotal) {
        this.noteTotal = noteTotal;
    }

    public List<MetricResponse> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<MetricResponse> metrics) {
        this.metrics = metrics;
    }
}
