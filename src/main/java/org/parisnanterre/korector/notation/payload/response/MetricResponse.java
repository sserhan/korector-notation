package org.parisnanterre.korector.notation.payload.response;

import java.io.Serializable;

public class MetricResponse implements Serializable {
    private Long id;
    private String nom;
    private String alias;
    private String unite;
    private String provenance;
    private Double poids;
    private Double valeur;
    private Double valeurComparaison;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public String getProvenance() {
        return provenance;
    }

    public void setProvenance(String provenance) {
        this.provenance = provenance;
    }

    public Double getPoids() {
        return poids;
    }

    public void setPoids(Double poids) {
        this.poids = poids;
    }

    public Double getValeur() {
        return valeur;
    }

    public void setValeur(Double valeur) {
        this.valeur = valeur;
    }

    public Double getValeurComparaison() {
        return valeurComparaison;
    }

    public void setValeurComparaison(Double valeurComparaison) {
        this.valeurComparaison = valeurComparaison;
    }
}
