package org.parisnanterre.korector.notation.repository;

import org.parisnanterre.korector.notation.entity.Metric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MetricRepository extends JpaRepository<Metric,Long> {
    List<Metric> findByProvenanceAndGlobal(String Provenance, boolean global);
    List<Metric> findByGlobal(boolean global);
}
